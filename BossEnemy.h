// BossEnemy.h
#ifndef BOSS_ENEMY_H
#define BOSS_ENEMY_H

#include <string>
#include "Character.h"

class BossEnemy {
private:
    std::string name_;
    int health_;

public:
    BossEnemy(const std::string& name, int health);
    void TakeDamage(int damage);
    bool IsDefeated() const;
    int GetHealth() const;
};

#endif // BOSS_ENEMY_H
