#include "Inventory.h"
#include <algorithm>
#include <iostream>

void Inventory::addItem(const Item& item) {
    items.push_back(item);
}

bool Inventory::dropItem(const std::string& itemName) {
    auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& i) { return i.GetName() == itemName; });
    if (it != items.end()) {
        items.erase(it);
        return true;
    }
    return false;
}

void Inventory::displayInventory() const {
    if (items.empty()) {
        std::cout << "Inventory is empty." << std::endl;
    } else {
        std::cout << "Inventory:" << std::endl;
        for (const auto& item : items) {
            std::cout << "- " << item.GetName() << std::endl;
        }
    }
}

const std::vector<Item>& Inventory::getItems() const {
    return items;
}
