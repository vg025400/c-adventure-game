// BossEnemy.cpp
#include "BossEnemy.h"

BossEnemy::BossEnemy(const std::string& name, int health) : name_(name), health_(health) {
    // Constructor implementation
}

void BossEnemy::TakeDamage(int damage) {
    health_ -= damage;
    if (health_ < 0) {
        health_ = 0;
    }
}

bool BossEnemy::IsDefeated() const {
    return health_ <= 0;
}

int BossEnemy::GetHealth() const {
    return health_;
}
