#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <map>
#include <vector>
#include "Item.h"


class Room {
private:
    std::string name;
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;


public:
    Room(const std::string& name, const std::string& description);
    std::string GetName() const;
    void AddExitRoom(const std::string& direction, Room* room);
    Room* GetExit(const std::string& direction);
    void AddItem(const Item& item);
    void RemoveItem(const Item& item);
    const std::string& GetDescription() const;
    const std::vector<Item>& GetItems() const;
    Item TakeItem(const std::string& itemName); // Take an item from the room
};

#endif // ROOM_H
