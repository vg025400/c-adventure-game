#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <vector>
#include "Item.h"

class Room;

class Player {
private:
    std::string name_;
    int health_;
    Room* location_;
    std::vector<Item> inventory_;


public:
    Player(const std::string& name, int health);

    void SetLocation(Room* room);
    Room* GetLocation() const;
    int GetHealth() const;

    void Move(const std::string& direction);
    void LookAround();
    void PickUpItem(const std::string& itemName);
    void DropItem(const std::string& itemName);
    void displayInventory();


    // New function to check conditions before moving to a new room
    bool CanMoveTo(Room* nextRoom) const;

    bool HasKey() const;
    bool HasLantern() const;
    bool HasDiamond() const;
    bool HasSword() const;
    bool HasItem(const std::string& itemName) const; // Declare HasItem function
};

#endif // PLAYER_H
