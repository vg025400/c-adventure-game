#include "CommandInterpreter.h"
#include <iostream>
#include <sstream>
#include "Player.h"
#include "BossEnemy.h"


CommandInterpreter::CommandInterpreter(Player* player, Area* area) : player_(player), area_(area), npcInteracted_(false) {
    // Initialize the CommandInterpreter object with the provided Player and Area objects
}

void CommandInterpreter::interpretCommand(const std::string& command) {
    // Parse the command and extract the action and direction
    std::istringstream iss(command);
    std::string action, itemName;
    iss >> action;

    // Execute the corresponding action based on the parsed command
    if (action == "move") {
        std::string direction;
        iss >> direction;
        if (checkMoveValidity(direction)) {
            player_->Move(direction);
            checkGameCompletion(); // Check for game completion after moving
            checkNPCInteraction(); // Check for NPC interaction after moving
            // Check if the player entered the dark tunnel and start boss fight automatically
            if (player_->GetLocation()->GetName() == "darkTunnel") {
                startBossFight();
            }
        }
    } else if (action == "look") {
        player_->LookAround();
        checkNPCInteraction(); // Check for NPC interaction after looking around
    } else if (action == "pick" && iss >> action && action == "up" && iss >> itemName) {
        player_->PickUpItem(itemName);
    } else if (action == "interact") {
        InteractWithNPC(); // Interact with NPC
    } else if (action == "drop") {
        if (!(iss >> itemName)) {
            std::cout << "Usage: drop <item_name>" << std::endl;
            return;
        }
        player_->DropItem(itemName); // Drop item command
    } else if (action == "inventory") {
        player_->displayInventory();
    } else if (action == "attack") {
        attackBoss(); // Attack boss with sword
    } else {
        std::cout << "Unknown command: " << command << std::endl;
    }
}


bool CommandInterpreter::checkMoveValidity(const std::string& direction) {
    // Check if the player has the required items to move in a certain direction
    if (direction == "north" && player_->GetLocation()->GetName() == "hallway" && !player_->HasKey()) {
        std::cout << "You need the key to enter the treasure room." << std::endl;
        return false;
    } else if (direction == "west" && player_->GetLocation()->GetName() == "treasureRoom" && !player_->HasLantern()) {
        std::cout << "You need the lantern to enter the dark tunnel." << std::endl;
        return false;
    } else if (direction == "west" && player_->GetLocation()->GetName() == "darkTunnel") {
        if (bossEnemy_ != nullptr) {
            std::cout << "You can't leave yet! Defeat the boss first." << std::endl;
            return false;
        } else {
            // Allow moving west if there is no active boss fight
            return true;
        }
    }
    return true;
}

void CommandInterpreter::checkGameCompletion() {
    // Check if the player has reached the outside room
    if (player_->GetLocation()->GetName() == "outside") {
        if (player_->HasDiamond()) {
            std::cout << "Congratulations! You have completed the game and retrieved the diamond!" << std::endl;
        } else {
            std::cout << "Congratulations! You have completed the game, but it feels like you forgot something..." << std::endl;
        }
        gameCompleted_ = true;
    }
}


void CommandInterpreter::checkNPCInteraction() {
    // Check if the player is in the same room as the NPC
    Room* playerRoom = player_->GetLocation();
    Room* npcRoom = area_->GetRoom("hallway"); // Change to the NPC's room name
    if (playerRoom == npcRoom && !npcInteracted_) {
        npcInteracted_ = true; // Mark NPC interaction as true to prevent repeated interaction
    }
}

void CommandInterpreter::InteractWithNPC() {
    // Check if the player is in the same room as the NPC
    Room* playerRoom = player_->GetLocation();
    Room* npcRoom = area_->GetRoom("hallway"); // Change to the NPC's room name
    if (playerRoom == npcRoom && npcInteracted_) {
        std::cout << "You start a conversation with Mr. Chubwewe." << std::endl;
        std::cout << "Mr. Chubwewe: How you dey? My name is Mr Chubwewe, That door over there leads to the treasure room. Remember the commands for this are game are 'look','attack','pick up itemname' and 'move direction'.You might miss something important if you forget.Good Luck." << std::endl;
        std::cout << "Conversation ends." << std::endl;
    } else {
        std::cout << "There is no one to interact with." << std::endl;
    }
}

void CommandInterpreter::startBossFight() {
    // Get the player's current location
    Room* playerRoom = player_->GetLocation();
    // Get the dark tunnel room
    Room* darkTunnelRoom = area_->GetRoom("darkTunnel");
    // Check if the player is in the dark tunnel
    if (playerRoom == darkTunnelRoom) {
        if (!bossEnemy_) { // If there is no boss currently
            if (player_->HasItem("Sword")) { // Check if the player has a sword
                bossEnemy_ = new BossEnemy("Boss", 200); // Create a new boss enemy
                std::cout << "As you enter the tunnel,you catch the attention of a fearsome monster,it begins charging at you! Prepare for battle!" << std::endl;
                std::cout << "The monster has " << bossEnemy_->GetHealth() << " health. Attack it with your sword using the 'attack' command!" << std::endl;
            } else {
                std::cout << "As you enter the dark tunnel, a fearsome monster spots you and charges at you, but you have no weapon to defend yourself." << std::endl;
                std::cout << "The monster kills you. Game over!" << std::endl;
                std::cout << "Player's Health: 0" << std::endl;
                exit(0); // End the game
            }
        } else {
            if (!bossEnemy_->IsDefeated()) { // If the boss is not defeated
                std::cout << "You are already in a boss fight!" << std::endl;
            } else {
                std::cout << "The boss has already been defeated! You can proceed." << std::endl;
            }
        }
    } else {
        std::cout << "You are not in the dark tunnel!" << std::endl;
    }
}


void CommandInterpreter::attackBoss() {
    if (bossEnemy_) {
        if (player_->HasSword()) {
            bossEnemy_->TakeDamage(50); // Adjust damage as needed
            std::cout << "You attack the boss with your sword! The boss's health is now: " << bossEnemy_->GetHealth() << std::endl;
            if (bossEnemy_->IsDefeated()) { // Check if the boss is defeated
                std::cout << "You defeated the boss! You can now proceed." << std::endl;
                delete bossEnemy_;
                bossEnemy_ = nullptr; // Set bossEnemy_ to nullptr after defeating the boss
            }
        } else {
            std::cout << "You need a sword to attack the boss!" << std::endl;
        }
    } else {
        std::cout << "There is no boss to attack!" << std::endl;
    }
}

bool CommandInterpreter::isGameCompleted() const {
    return gameCompleted_;
}
