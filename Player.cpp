#include "Player.h"
#include <iostream>
#include <algorithm> // Include algorithm header for std::find_if
#include "Item.h"
#include "Room.h"

Player::Player(const std::string& name, int health) : name_(name), health_(health), location_(nullptr)  {}

void Player::SetLocation(Room* room) {
    location_ = room;
}

Room* Player::GetLocation() const {
    return location_;
}

int Player::GetHealth() const {
    return health_;
}

void Player::Move(const std::string& direction) {
    Room* nextRoom = location_->GetExit(direction);
    if (nextRoom != nullptr && CanMoveTo(nextRoom)) { // Check if the player can move to the next room
        SetLocation(nextRoom);
        std::cout << "You move " << direction << "." << std::endl;

        // Check if the player is entering the dark tunnel
        if (nextRoom->GetName() == "darkTunnel") {
            std::cout << "You sense heavy bloodlust emitting from the tunnel." << std::endl;
        }
    } else {
        std::cout << "You can't go " << direction << "." << std::endl;
    }
}

void Player::LookAround() {
    std::cout << "You look around the room." << std::endl;
    const std::vector<Item>& items = location_->GetItems();
    if (!items.empty()) {
        std::cout << "You see the following items in the room:" << std::endl;
        for (const auto& item : items) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }
    } else {
        std::cout << "There are no items in this room." << std::endl;
    }

    // Check if the player is in the hallway and update the description
    if (location_->GetName() == "hallway") {
        std::cout << "There's a man standing in the corner." << std::endl;
    }
}

void Player::PickUpItem(const std::string& itemName) {
    const std::vector<Item>& items = location_->GetItems();
    auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
    });
    if (it != items.end()) {
        // Found the item, add it to the player's inventory
        inventory_.push_back(*it);
        std::cout << "You pick up the " << itemName << "." << std::endl;
        // Remove the picked-up item from the room
        location_->RemoveItem(*it);
    } else {
        std::cout << "The item '" << itemName << "' is not in this room." << std::endl;
    }
}

void Player::DropItem(const std::string& itemName) {
    auto it = std::find_if(inventory_.begin(), inventory_.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
    });

    if (it != inventory_.end()) {
        location_->AddItem(*it);
        inventory_.erase(it);
        std::cout << "You dropped the " << itemName << "." << std::endl;
    } else {
        std::cout << "No such item in inventory." << std::endl;
    }
}

void Player::displayInventory() {
    std::cout << "Inventory:" << std::endl;
    for (const auto& item : inventory_) {
        std::cout << "- " << item.GetName() << std::endl;
    }
}


bool Player::CanMoveTo(Room* nextRoom) const {
    // Implement the conditions for moving to a new room
    if (nextRoom->GetName() == "treasureRoom") {
        // Check if the player has the key to enter the treasure room
        return std::find_if(inventory_.begin(), inventory_.end(), [](const Item& item) {
            return item.GetName() == "Key";
        }) != inventory_.end();
    } else if (nextRoom->GetName() == "darkTunnel") {
        // Check if the player has the lantern to enter the dark tunnel
        return std::find_if(inventory_.begin(), inventory_.end(), [](const Item& item) {
            return item.GetName() == "Lantern";
        }) != inventory_.end();
    } else {
        // Default: Allow movement to other rooms
        return true;
    }
}
bool Player::HasKey() const {
    for (const auto& item : inventory_) {
        if (item.GetName() == "Key") {
            return true;
        }
    }
    return false;
}

bool Player::HasLantern() const {
    for (const auto& item : inventory_) {
        if (item.GetName() == "Lantern") {
            return true;
        }
    }
    return false;
}

bool Player::HasDiamond() const {
    for (const auto& item : inventory_) {
        if (item.GetName() == "Diamond") {
            return true;
        }
    }
    return false;
}

bool Player::HasSword() const {
    for (const auto& item : inventory_) {
        if (item.GetName() == "Sword") {
            return true;
        }
    }
    return false;
}

bool Player::HasItem(const std::string& itemName) const {
    // Check if the player has the specified item in their inventory
    return std::find_if(inventory_.begin(), inventory_.end(), [&itemName](const Item& item) {
        return item.GetName() == itemName;
    }) != inventory_.end();
}

