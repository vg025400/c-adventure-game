#ifndef COMMAND_INTERPRETER_H
#define COMMAND_INTERPRETER_H

#include "Player.h"
#include "Area.h"
#include "BossEnemy.h"

class CommandInterpreter {
private:
    Player* player_;
    Area* area_;
    BossEnemy* bossEnemy_; // Declare bossEnemy_ as a member variable
    bool npcInteracted_;
    bool gameCompleted_;

public:
    CommandInterpreter(Player* player, Area* area);

    void interpretCommand(const std::string& command);
    bool isGameCompleted() const;
    void checkGameCompletion();

private:
    bool checkMoveValidity(const std::string& direction);
    void checkNPCInteraction();
    void InteractWithNPC();
    void startBossFight();
    void attackBoss();
};

#endif // COMMAND_INTERPRETER_H
