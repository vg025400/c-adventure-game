#include "Item.h"
#include <iostream>

Item::Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}

void Item::Interact() const {
    std::cout << "You interact with the " << name << ". " << description << std::endl;
}

const std::string& Item::GetName() const {
    return name;
}

const std::string& Item::GetDescription() const {
    return description;
}
