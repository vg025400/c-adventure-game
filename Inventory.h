#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include "Item.h"

class Inventory {
private:
    std::vector<Item> items;

public:
    void addItem(const Item& item);
    bool dropItem(const std::string& itemName);
    void displayInventory() const;
    const std::vector<Item>& getItems() const;
};

#endif // INVENTORY_H
