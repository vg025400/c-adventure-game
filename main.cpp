#include <iostream>
#include <limits>
#include "Area.h"
#include "Player.h"
#include "CommandInterpreter.h"

int main() {
    // Create an instance of the Area class
    Area gameArea;

    // Load the game map from a text file
    gameArea.LoadMapFromFile("game_map.txt");

    // Create a Player
    Player player("Alice", 100);

    // Set the player's starting location
    Room *startRoom = gameArea.GetRoom("startRoom");
    if (startRoom) {
        player.SetLocation(startRoom);
    } else {
        std::cerr << "Start room not found!" << std::endl;
        return 1;
    }

    // Create a CommandInterpreter object with the player and gameArea objects
    CommandInterpreter interpreter(&player, &gameArea);

    // Display current location and player's health before entering the game loop
    std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
    std::cout << "Player's Health: " << player.GetHealth() << std::endl;

    // Game loop
    while (!interpreter.isGameCompleted()) {
        // Display available options
        std::cout << "Options: ";
        std::cout << "1. Enter command | ";
        std::cout << "2. Help | ";
        std::cout << "3. Quit" << std::endl;

        // Get player's choice
        int choice;
        while (true) {
            std::cout << "Enter your choice: ";
            std::cin >> choice;

            // Check if the choice is valid
            if (choice >= 1 && choice <= 3) {
                break;
            } else {
                std::cout << "Invalid choice. Please enter a number between 1 and 3." << std::endl;
                // Clear input buffer
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }

        // Process player's choice
        if (choice == 1) {
            std::string command;
            std::cout << "> ";
            std::cin.ignore();
            std::getline(std::cin, command);
            interpreter.interpretCommand(command); // Interpret the command and execute corresponding action
        } else if (choice == 2) {
            // Display help message
            std::cout << "Commands:" << std::endl;
            std::cout << "1. move <direction>: Move to a different room." << std::endl;
            std::cout << "2. look: Examine the current room." << std::endl;
            std::cout << "3. pick up <item>: Pick up an item in the room." << std::endl;
            std::cout << "4. interact: Interact with NPCs." << std::endl;
            std::cout << "5. drop <item>: Drop an item from your inventory." << std::endl;
            std::cout << "6. inventory: View your inventory." << std::endl;
            std::cout << "7. fight: Initiate a fight with a boss enemy." << std::endl;
            std::cout << "8. attack: Attack a boss enemy during a fight." << std::endl;
        } else if (choice == 3) {
            // Quit the game
            std::cout << "Goodbye!" << std::endl;
            break;
        }

        // Display current location and health before checking for game completion
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
        std::cout << "Player's Health: " << player.GetHealth() << std::endl;


    }

    return 0;
}
