Room startRoom You are in a dimly lit room.
Room hallway You are in a long hallway.
Room treasureRoom You have entered a treasure room!
Room darkTunnel You find yourself in a dark tunnel.
Room outside You are outside, surrounded by trees.

// Define exits between rooms
Define startRoom hallway north
Define hallway startRoom south
Define hallway treasureRoom north
Define treasureRoom hallway south
Define treasureRoom darkTunnel west
Define darkTunnel treasureRoom east
Define darkTunnel outside west
Define outside darkTunnel east

// Create Items
Create Key "A shiny key that looks important." startRoom
Create Sword "A sharp sword with a golden hilt." treasureRoom
Create Lantern "A lantern to light up the darkness." hallway
Create Diamond "A purple diamond that looks valuable." treasureRoom