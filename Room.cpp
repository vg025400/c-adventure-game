#include "Room.h"
#include <iostream>
#include <algorithm>

Room::Room(const std::string& name, const std::string& description)
        : name(name), description(description) {}

std::string Room::GetName() const {
    return name;
}

void Room::AddExitRoom(const std::string& direction, Room* room) {
    exits[direction] = room;
}

Room* Room::GetExit(const std::string& direction) {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return it->second;
    }
    std::cout << "Adjacent room not found: " << direction << std::endl;
    return nullptr;
}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::RemoveItem(const Item& item) {
    auto it = std::find_if(items.begin(), items.end(), [&item](const Item& i) { return i.GetName() == item.GetName(); });
    if (it != items.end()) {
        items.erase(it);
    } else {
        std::cout << "Item not found in room: " << item.GetName() << std::endl;
    }
}

const std::string& Room::GetDescription() const {
    return description;
}

const std::vector<Item>& Room::GetItems() const {
    return items;
}


Item Room::TakeItem(const std::string& itemName) {
    auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& item) { return item.GetName() == itemName; });
    if (it != items.end()) {
        Item item = *it;
        items.erase(it);
        return item;
    } else {
        std::cout << "Item not found in room: " << itemName << std::endl;
        return Item("", "");
    }
}